<?php
namespace modele\metier;

/**
 * Description d'une representation
 * 
 * @author eleve
 */
class Representation {
    /** id de la representation
     * @var string
     */
    private $id;
    /** groupe concerné par la représentation
     * @var string
     */
    private $groupe;
    /**
     * lieu de la représentation
     * @var string
     */
    private $lieu;
    /**
     * date de la représentation
     * @var date
     */
    private $date;
    /**
     * heure de début de la représentation
     * @var heureDebut
     */
    private $heureDebut;
    /**
     * heure de fin de la représentation
     * @var heureFin
     */
    private $heureFin;
    
    
    function __construct($id, string $groupe, string $lieu, $date, $heureDebut, $heureFin) {
        $this->id = $id;
        $this->groupe = $groupe;
        $this->lieu = $lieu;
        $this->date = $date;
        $this->heureDebut = $heureDebut;
        $this->heureFin = $heureFin;
    }
    
    function getId(){
        return $this->id;
    }
    
    function getGroupe() {
        return $this->groupe;
    }

    function getLieu() {
        return $this->lieu;
    }

    function getDate()  {
        return $this->date;
    }
    
    function getHeureDebut()  {
        return $this->heureDebut;
    }
    
    function getHeureFin()  {
        return $this->heureFin;
    }
    
    function setId(string $id){
        $this->id = $id;
    }
    
    function setGroupe(string $groupe) {
        $this->groupe = $groupe;
    }

    function setLieu(string $lieu) {
        $this->lieu = $lieu;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setHeureDebut($heureDebut){
        $this->heureDebut = $heureDebut;
    }
    
    function setHeureFin($heureFin){
        $this->heureFin = $heureFin;
    }
    
}
